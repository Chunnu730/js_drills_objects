const obj = require('../testObject.js');
const mapObject = require('../mapObject.js');
const cb = (key) => obj[key] + 'a';
const result = mapObject(obj, cb);
const expectedResult = { name: 'Bruce Waynea', age: '36a', location: 'Gothama' };
if (JSON.stringify(expectedResult) === JSON.stringify(result)) {
    console.log('Test passed');
    console.log(result);
} else {
    console.log('Test failed');
}