const obj = require('../testObject.js');
const invert = require('../invert.js');
const result = invert(obj);
const expectedResult = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };
if (JSON.stringify(result) === JSON.stringify(expectedResult)) {
    console.log('Test Passed');
    console.log(result);
}
else
    console.log('Test failed');