const obj = require('../testObject.js');
const values = require('../values.js');
const result = values(obj);
const expectedResult = ['Bruce Wayne', 36, 'Gotham'];
if (JSON.stringify(expectedResult) === JSON.stringify(result)) {
    console.log("Test passed");
    console.log(result);
} else {
    console.log('Test failed');
}
