const obj = require('../testObject.js');
const keys = require('../keys.js');
const result = keys(obj);
const expectedResult = ['name', 'age', 'location'];
if (JSON.stringify(expectedResult) === JSON.stringify(result)) {
    console.log('Test passed');
    console.log(result);
}
else {
    console.log('Test failed');
}