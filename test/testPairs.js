const obj = require('../testObject.js');
const pairs = require('../pairs.js');
const result = pairs(obj);
const expectedResult = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']];
if (JSON.stringify(expectedResult) === JSON.stringify(result)) {
    console.log('Test passed');
    console.log(result);
}
else {
    console.log('Test failed');
}