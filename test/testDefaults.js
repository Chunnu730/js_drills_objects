const obj = require('../testObject.js');
const defaults = require('../defaults.js');
const result = defaults(obj, { age: 26, degree: 'Btech' });
const expectedResult = { name: 'Bruce Wayne', age: 36, location: 'Gotham', degree: 'Btech' };
if (JSON.stringify(expectedResult) === JSON.stringify(result)) {
    console.log('Test passed');
    console.log(result);
} else {
    console.log('Test failed');
}
