function pairs(obj) {
    let pairsValue = [];
    for (let key in obj) {
        pairsValue.push([key, obj[key]]);
    }
    return pairsValue;
}

module.exports = pairs;