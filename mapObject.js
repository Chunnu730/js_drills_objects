function mapObject(obj, cb) {
    let newArray = {};
    for (let key in obj) {
        newArray[key] = cb(key);
    }
    return newArray;
}

module.exports = mapObject;