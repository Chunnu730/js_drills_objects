function keys(obj) {
    let keyValue = [];
    for (let key in obj) {
        keyValue.push(key);
    }
    return keyValue;
}

module.exports = keys;
