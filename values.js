function values(obj) {
    let values = [];
    for (let key in obj) {
        if (typeof (obj[key]) !== 'function') {
            values.push(obj[key]);
        }
    }
    return values;
}

module.exports = values;